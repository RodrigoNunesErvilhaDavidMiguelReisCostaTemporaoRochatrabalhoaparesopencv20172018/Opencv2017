
























import cv2
import numpy as np

MIN_MATCH_COUNT = 30

detector = cv2.SIFT()

flannParam = dict(algorithm=0, tree=5)
flann = cv2.FlannBasedMatcher(flannParam, {})

Img = cv2.imread("P_20170613_111125.jpeg", 0)
trainKP, trainDesc = detector.detectAndCompute(Img, None)

cap = cv2.VideoCapture(0)

while True:
    ret, frame = cap.read()
    fgmask = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    queryKP, queryDesc = detector.detectAndCompute(fgmask, None)
    matches = flann.knnMatch(queryDesc, trainDesc, k=2)

    goodMatch = []
    for m, n in matches:
        if m.distance < 0.75*n.distance:
            goodMatch.append(m)

    if len(goodMatch) > MIN_MATCH_COUNT:
        tp = []
        qp = []
        for m in goodMatch:
            tp.append(trainKP[m.trainIdx].pt)
            qp.append(queryKP[m.queryIdx].pt)
        tp, qp = np.float32((tp, qp))
        H, status = cv2.findHomography(tp, qp, cv2.RANSAC, 3.0)
        h, w = Img.shape
        trainBorder = np.float32([[[0, 0], [0, h-1], [w-1, h-1], [w-1, 0]]])
        queryBorder = cv2.perspectiveTransform(trainBorder, H)
        cv2.polylines(frame, [np.int32(queryBorder)], True, (0, 255, 0), 5)
    else:
        print("Not Enough match found- %d/%d" % (len(goodMatch), MIN_MATCH_COUNT))
    cv2.imshow('result', frame)
    if cv2.waitKey(10) == ord('q'):
        break
cap.release()
cv2.destroyAllWindows()
